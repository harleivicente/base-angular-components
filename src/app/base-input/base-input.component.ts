import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-base-input',
  templateUrl: './base-input.component.html',
  styleUrls: ['./base-input.component.css']
})
export class BaseInputComponent implements OnInit {
  @Input() formGroupHandle: FormGroup;
  @Input() showErrors: boolean; 

  control: FormControl;
  config = {
    label: 'Cpf',
    key: 'cpf',
    placeholder: 'Escreva seu CPF'
  };
  validators: ValidatorFn[] = [Validators.required, Validators.minLength(12)];
  mask = [/[1-9]/, /[1-9]/, /[1-9]/, /[1-9]/, /[1-9]/, /[1-9]/, /[1-9]/, /[1-9]/, /[1-9]/, '-', /[1-9]/, /[1-9]/];

  constructor() {}
  
  ngOnInit() {
    this.control = new FormControl('', this.validators);
    this.formGroupHandle.addControl(this.config.key, this.control);
  }

  get hasError() {
    return this.control.errors && (this.control.touched || this.showErrors);
  }

  get hasSuccess() {
    return !this.control.errors && (this.control.touched || this.showErrors);
  }

}
