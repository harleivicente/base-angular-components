import { Component, ViewChild } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  formGroup = new FormGroup({});
  title = 'app';
  @ViewChild('myForm') myForm: NgForm;

  ngOnInit() {
  }
  
  onSubmit(){
    console.log(this.formGroup.getRawValue());
  }  
}
